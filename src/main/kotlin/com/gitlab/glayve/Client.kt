package com.gitlab.glayve

import com.mojang.authlib.Agent.MINECRAFT
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService
import com.mojang.authlib.yggdrasil.YggdrasilUserAuthentication
import net.minecraft.client.Minecraft
import java.net.Proxy.NO_PROXY
import java.nio.file.Paths
import java.util.*

object Client {
    @JvmStatic fun main(args: Array<String>) {
        val path = Paths.get("", "natives").toAbsolutePath()
        System.setProperty("org.lwjgl.librarypath", path.toString())
        val token = UUID.randomUUID().toString()
        val service = YggdrasilAuthenticationService(NO_PROXY, token)
        YggdrasilUserAuthentication(service, MINECRAFT).apply {
//            setUsername(args[0]); setPassword(args[1]); logIn()
            Minecraft(this, 1920, 1080).start("localhost", 25577)
        }
    }
}