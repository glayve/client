plugins {
    id("org.jetbrains.kotlin.jvm").version("1.4.0")
    id("com.github.johnrengelman.shadow").version("6.0.0")
}

group = "com.gitlab.glayve"
version = "1.0.0"

repositories {
    maven("https://repo1.maven.org/maven2")
    maven("https://libraries.minecraft.net")
}

dependencies {
    //--Google--
    implementation("com.google.guava:guava:17.0")
    implementation("com.google.code.gson:gson:2.2.4")

    //--Java--
    implementation("net.java.dev.jna:jna:3.4.0")
    implementation("net.java.dev.jna:platform:3.4.0")
    implementation("net.java.jinput:jinput:2.0.5")
    implementation("net.java.jinput:jinput-platform:2.0.5")
    implementation("net.java.jutils:jutils:1.0.0")

    //--Apache--
    implementation("org.apache.commons:commons-compress:1.8.1")
    implementation("org.apache.commons:commons-lang3:3.3.2")
    implementation("org.apache.httpcomponents:httpclient:4.3.3")
    implementation("org.apache.httpcomponents:httpcore:4.3.2")
    implementation("org.apache.logging.log4j:log4j-api:2.0-beta9")
    implementation("org.apache.logging.log4j:log4j-core:2.0-beta9")

    //--Mojang--
    implementation("com.mojang:authlib:1.5.21")
    implementation("com.mojang:netty:1.6")
    implementation("com.mojang:realms:1.7.39")

    //--Paulscode--
    implementation("com.paulscode:codecjorbis:20101023")
    implementation("com.paulscode:codecwav:20101023")
    implementation("com.paulscode:libraryjavasound:20101123")
    implementation("com.paulscode:librarylwjglopenal:20100824")
    implementation("com.paulscode:soundsystem:20120107")

    //--Commons--
    implementation("commons-codec:commons-codec:1.9")
    implementation("commons-io:commons-io:2.4")
    implementation("commons-logging:commons-logging:1.1.3")

    //--LWJGL--
    implementation("org.lwjgl.lwjgl:lwjgl:2.9.4-nightly-20150209")
    implementation("org.lwjgl.lwjgl:lwjgl_util:2.9.4-nightly-20150209")


    //--Other--
    implementation("tv.twitch:twitch:6.5")
    implementation("io.netty:netty-all:4.0.23.Final")
    implementation("net.sf.jopt-simple:jopt-simple:4.6")
    implementation("oshi-project:oshi-core:1.1")
    implementation("com.ibm.icu:icu4j-core-mojang:51.2")
}

tasks.shadowJar {
    archiveFileName.set("${project.name}.jar")
    destinationDirectory.set(file("/Client"))
    manifest.attributes["Main-Class"] = "com.gitlab.glayve.Client"
}

tasks.build { dependsOn(tasks.shadowJar) }